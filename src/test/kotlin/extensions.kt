fun String.asResource(work: (String) -> Unit) {
    val content = DayOne::class.java.getResource(this).readText()
    work(content)
}