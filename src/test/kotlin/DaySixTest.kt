import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class DaySixTest {
    @Test
    fun `should validate for example`() {
        val sample = "0\t5\t10\t0\t11\t14\t13\t4\t11\t8\t8\t7\t1\t4\t12\t11".split("\t").map(String::toInt)

        assertThat(DaySix.redistribute(sample), `is`(4))
    }
}