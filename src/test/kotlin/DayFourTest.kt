import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class DayFourTest {
    @ParameterizedTest(name = "for {0} should validate to {1}")
    @CsvSource(value = arrayOf(
            "aa bb cc dd ee, true",
            "aa bb cc dd aa, false",
            "aa bb cc dd aaa, true"
    ))
    fun `should validate passphare`(passphrase: String, expectedValidity: Boolean) {
        assertThat(DayFour.validate(passphrase), `is`(expectedValidity))
    }

    @ParameterizedTest(name = "for {0} should validate to {1}")
    @CsvSource(value = arrayOf(
            "abcde fghij, true",
            "abcde xyz ecdab, false",
            "a ab abc abd abf abj, true",
            "iiii oiii ooii oooi oooo, true",
            "oiii ioii iioi iiio, false"
    ))
    fun `should validate passphare without anagrams`(passphrase: String, expectedValidity: Boolean) {
        assertThat(DayFour.validateStrict(passphrase), `is`(expectedValidity))
    }

    @Test
    fun `should validate for challenge`() {
        "dayFourPassphrases.txt".asResource {
            assertThat(it.split("\n").map(DayFour::validate).filter { it }.size, `is`(386))
        }
    }

    @Test
    fun `should validate with anagrams for challenge`() {
        "dayFourPassphrases.txt".asResource {
            assertThat(it.split("\n").map(DayFour::validateStrict).filter { it }.size, `is`(208))
        }
    }
}