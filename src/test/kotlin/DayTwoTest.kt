import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class DayTwoTest {
    private fun String.toPuzzle() = split("\n").filter { it.isNotBlank() }.map { it.trim().split("\\s+".toRegex()).map { it.toInt() } }

    @Test
    fun `should solve checksum for first puzzle`() {
        val sample = """
        5 1 9 5
        7 5 3
        2 4 6 8
        """.trimIndent()

        assertThat(DayTwo.checksum(sample.toPuzzle(), DayTwo::calculateDifferenceMaxAndMin), `is`(18))
    }

    @Test
    fun `should solve checksum for second puzzle`() {
        val sample = """
        5 9 2 8
        9 4 7 3
        3 8 6 5
        """.trimIndent()

        assertThat(DayTwo.checksum(sample.toPuzzle(), DayTwo::calculateEvenlyDivisible), `is`(9))
    }
}