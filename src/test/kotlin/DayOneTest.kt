import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class DayOneTest {
    private fun String.toPuzzle() = this.chunked(1).map(String::toInt)

    @ParameterizedTest(name = "captcha of {0} should be {1}")
    @CsvSource(value = arrayOf(
        "1122, 3",
        "1111, 4",
        "1234, 0",
        "91212129, 9"
    ))
    fun `for first puzzle`(puzzle: String, answer: Int) {
        assertThat(DayOne.captcha(puzzle.toPuzzle(), DayOne::nextElement), `is`(answer))
    }

    @ParameterizedTest(name = "captcha of {0} should be {1}")
    @CsvSource(value = arrayOf(
        "1212, 6",
        "1221, 0",
        "123425, 4",
        "123123, 12",
        "12131415, 4"
    ))
    fun `for second puzzle`(puzzle: String, answer: Int) {
        assertThat(DayOne.captcha(puzzle.toPuzzle(), DayOne::nextElementHalfwayAcrossCenter), `is`(answer))
    }
}