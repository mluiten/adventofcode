import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

class DayFiveTest {
    @Test
    fun `should validate for example`() {
        "dayFiveSample.txt".asResource {
            assertThat(DayFive.solve(it.split("\n").map(String::toInt)) { 1 }, `is`(5))
        }
    }

    @Test
    fun `should validate for challenge`() {
        "dayFive.txt".asResource {
            assertThat(DayFive.solve(it.split("\n").map(String::toInt), { 1 }), `is`(372671))
        }
    }

    @Test
    fun `should validate for example part two`() {
        "dayFiveSample.txt".asResource {
            assertThat(DayFive.solve(it.split("\n").map(String::toInt)) { if (it >= 3) -1 else 1 }, `is`(10))
        }
    }

    @Test
    fun `should validate for challenge part two`() {
        "dayFive.txt".asResource {
            assertThat(DayFive.solve(it.split("\n").map(String::toInt)) { if (it >= 3) -1 else 1}, `is`(25608480))
        }
    }
}