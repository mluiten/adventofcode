import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class DayThreeTest {
    @ParameterizedTest(name = "access point {0} should be carried in {1} steps")
    @CsvSource(value = arrayOf(
            "1, 0",
            "9, 2",
            "12, 3",
            "23, 2",
            "1024, 31",
            "265149, 438"
    ))
    fun `should solve checksum for first puzzle`(location: Int, expectedSteps: Int) {
        assertThat(DayThree.distance(location), `is`(expectedSteps))
    }

    @ParameterizedTest(name = "access point {0} should be carried in {1} steps")
    @CsvSource(value = arrayOf(
            "1, 0",
            "9, 2",
            "12, 3",
            "23, 2",
            "1024, 31",
            "265149, 438"
    ))
    fun `should solve for first puzzle naievely`(location: Int, expectedSteps: Int) {
        var index = 2;
        assertThat(DayThree.distanceNaive(location, {_, _ -> index++}), `is`(expectedSteps))
    }

    @ParameterizedTest(name = "access point {0} should be carried in {1} steps")
    @CsvSource(value = arrayOf(
            "1, 0",
            "9, 2",
            "12, 3",
            "23, 2",
            "1024, 31",
            "265149, 438"
    ))
    fun `should solve for second puzzle naievely`(location: Int, expectedSteps: Int) {
        var index = 2;
        assertThat(DayThree.distanceNaive(location, DayThree::basedOnNeighbors), `is`(expectedSteps))
    }
}