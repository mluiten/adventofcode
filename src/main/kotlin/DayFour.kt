object DayFour {
    fun validate(passphrase: String) : Boolean {
        val words = passphrase.split(" ")
        return words.distinct() == words
    }

    fun validateStrict(passphrase: String) : Boolean {
        val words = passphrase.split(" ")

        return words.flatMap { word ->
            (words - word).map { word.isPermutation(it) }
        }.none { it == true }
    }
}

private fun String.isPermutation(other: String): Any {
    val charCounter = this.groupBy { it }.mapValues { it.value.size }.toMutableMap()

    for(char in other) {
        val counterForCharacter = charCounter[char] ?: return false
        charCounter[char] = counterForCharacter - 1
    }

    return charCounter.all { it.value == 0 }
}
