object DayTwo {
    fun checksum(input: List<List<Int>>, strategy: (List<Int>) -> Int) = input
                .map(strategy)
                .sum()

    fun calculateDifferenceMaxAndMin(numbers: List<Int>): Int =
        numbers.max()!! - numbers.min()!!

    fun calculateEvenlyDivisible(numbers: List<Int>): Int =
        numbers
            .flatMap { i -> (numbers - i).filter { i % it == 0 }.map { i / it } }
            .single()
}