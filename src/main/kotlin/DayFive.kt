object DayFive {
    fun solve(instructions: List<Int>, afterJump: (Int) -> Int): Int {
        val program = instructions.toMutableList()

        var currentPosition = 0
        var steps = 0
        while (currentPosition < program.size) {
            val jumpSize = afterJump.invoke(program[currentPosition])

            program[currentPosition] += jumpSize
            currentPosition += (program[currentPosition] - jumpSize)

            steps++
        }

        return steps
    }
}