import java.lang.Math.*
import kotlin.math.floor

typealias Coordinate = Pair<Int, Int>

object DayThree {

    fun distance(location: Int): Int {
        val distanceToLocation = findNearestSqrt(location - 1)

        val distanceToCenterOfSquare = (distanceToLocation * distanceToLocation downTo 0 step maxOf(1, (distanceToLocation - 1)))
                .map { abs(location - it) }
                .min() ?: 0

        return distanceToLocation - 1 - distanceToCenterOfSquare
    }

    fun distanceNaive(location: Int, locationValueFn: (Coordinate, Map<Coordinate, Int>) -> Int): Int? {
        val memory = mutableMapOf<Coordinate, Int>()

        for (coordinate in generateCoordinates()) {
            if (coordinate == Coordinate(0, 0)) {
                memory[coordinate] = 1
            } else {
                memory[coordinate] = locationValueFn(coordinate, memory)
            }

            println ("[$coordinate] = ${memory[coordinate]}")
            if (memory[coordinate]!! > location) {
                return manhattan(coordinate)
            }
        }

        return null
    }

    private fun manhattan(coordinate: Coordinate): Int {
        return abs(coordinate.first) + abs(coordinate.second)
    }

    fun generateCoordinates() : Sequence<Coordinate> {
        var direction = Direction.RIGHT
        var stepSize = 2
        var stepsInDirection = 1

        return generateSequence(Coordinate(0, 0), { currentCoordinate ->
            if (stepsInDirection % stepSize == 0) {
                if (direction == Direction.UP || direction == Direction.DOWN) {
                    stepSize++
                }

                direction = direction.next()
                stepsInDirection = 1
            }

            stepsInDirection++
            println("Move from $currentCoordinate to ${currentCoordinate + direction}")
            currentCoordinate + direction
        })
    }

    fun basedOnNeighbors(location: Coordinate, memory: Map<Coordinate, Int>) : Int {
        return (memory[location + Coordinate(-1, 1)] ?: 0) +
               (memory[location + Coordinate( 0, 1)] ?: 0) +
               (memory[location + Coordinate( 1, 1)] ?: 0) +
               (memory[location + Coordinate(-1, 0)] ?: 0) +
               (memory[location + Coordinate( 1, 0)] ?: 0) +
               (memory[location + Coordinate(-1, -1)] ?: 0) +
               (memory[location + Coordinate( 0, -1)] ?: 0) +
               (memory[location + Coordinate( 1, -1)] ?: 0)
    }

    private operator fun Pair<Int, Int>.plus(other: Pair<Int, Int>) = Pair(this.first - other.first, this.second - other.second)
    private operator fun Pair<Int, Int>.plus(direction: Direction) = when(direction) {
        Direction.RIGHT -> Coordinate(this.first + 1, this.second)
        Direction.UP -> Coordinate(this.first, this.second + 1)
        Direction.DOWN -> Coordinate(this.first, this.second - 1)
        Direction.LEFT -> Coordinate(this.first - 1, this.second)
    }

    private enum class Direction {
        RIGHT,
        UP,
        LEFT,
        DOWN;

        fun next() = when(this) {
            RIGHT -> UP
            UP -> LEFT
            LEFT -> DOWN
            DOWN -> RIGHT
        }
    }

    private fun findNearestSqrt(location: Int): Int {
        return (location .. Int.MAX_VALUE).asSequence()
                .map(Int::toDouble)
                .map { sqrt(it) }
                .filter { floor(it) == it }
                .filter { it.isUneven() }
                .first()
                .toInt()
    }
}

private fun Double.isUneven() = this % 2 != 0.toDouble()
