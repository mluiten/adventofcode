object DaySix {
    fun redistribute(initial: List<Int>): Int {
        val seenBefore = mutableSetOf<List<Int>>()
        val memory = initial.toMutableList()
        var count = 0

        while (memory !in seenBefore) {
            seenBefore.plusAssign(memory)

            var highest = memory.max()!!
            var indexOfHighest = memory.indexOf(highest)

            memory[indexOfHighest] = 0
            while (highest > 0) {
                memory[(indexOfHighest + 1) % memory.size]++
                highest--
                indexOfHighest++
            }

            println(memory)
            count++
        }
        return count
    }
}