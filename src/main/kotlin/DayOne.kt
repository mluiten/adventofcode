object DayOne {
    fun captcha(puzzle: List<Int>, nextElement: (List<Int>, Int) -> Int) =
        puzzle.mapIndexed { index, number ->
            if (nextElement(puzzle, index) == number) number else 0
        }.sum()

    fun nextElement(puzzle: List<Int>, index: Int) =
            puzzle[(index + 1) % puzzle.size]

    fun nextElementHalfwayAcrossCenter(puzzle:List<Int>, index: Int) =
            puzzle[(index + puzzle.size / 2) % puzzle.size]
}